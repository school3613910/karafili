﻿using Learning.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace Learning.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        public IActionResult Index(string name, bool isAdmin)
        {
            ViewData["UserName"] = name;
            ViewData["isAdmin"] = isAdmin;

            var currentTime = DateTime.UtcNow;
            var cookieName = "dashboardStart";

            // Set the cookie with a 1-minute expiration time
            Response.Cookies.Append(cookieName, currentTime.ToString("o"),
                new CookieOptions { HttpOnly = true, Expires = currentTime.AddMinutes(1) });
            

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
