﻿using Learning.Models;
using Learning.NewFolder;
using Microsoft.AspNetCore.Mvc;


namespace Learning.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public CategoryController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public IActionResult Index()
        {
            IEnumerable<Category> objCategoryList = _applicationDbContext.category;
            return View(objCategoryList);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Category ct)
        {
            if(ModelState.IsValid)
            {
                _applicationDbContext.category.Add(ct);
                _applicationDbContext.SaveChanges();
                return RedirectToAction("Index");

            }
            return View(ct);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null || id == 0 )
            {
                return NotFound();
            }
            var categories = _applicationDbContext.category.Find(id);

            if(categories == null)
            {
                return NotFound();
            }

            return View(categories);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Category ct)
        {
            if (ModelState.IsValid)
            {
                _applicationDbContext.category.Update(ct);
                _applicationDbContext.SaveChanges();
                return RedirectToAction("Index");

            }
            return View(ct);
        }
    }
}
