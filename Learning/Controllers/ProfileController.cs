﻿using Learning.Models;
using Learning.NewFolder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Learning.Controllers
{
    public class ProfileController : Controller
    {

        private readonly ApplicationDbContext _context;

        public ProfileController(ApplicationDbContext context)
        {
            _context = context;
        }
        [HttpGet]
        public IActionResult Index(string nid)
        {
            var user = _context.login.FirstOrDefault(u => u.NID == nid);
            if (user == null)
            {
                return NotFound();
            }

            return PartialView(user);
        }

        [HttpPost]
        public IActionResult SaveProfile(LoginModel model)
        {

            // Retrieve the current user's login record from the database
            LoginModel user = _context.login.FirstOrDefault(u => u.Email == model.Email);

            // Update the user's profile with the new data
                user.Age = model.Age;
                user.Gender = model.Gender;
                user.CellPhone = model.CellPhone;
                user.City = model.City;
                user.State = model.State;

                // Save changes to the database
                _context.SaveChanges();



            return RedirectToAction("UserLogin", "Accounts", new
            {
                name = user.Name,
                isAdmin = user.isAdmin,
                nid = user.NID
            });
        }
    }
}
