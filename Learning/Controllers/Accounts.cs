﻿using Learning.Models;
using Learning.NewFolder;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Learning.Controllers
{
	public class AccountsController : Controller
	{
		private readonly ApplicationDbContext _applicationDbContext;
        private readonly IConfiguration _configuration;
        private readonly ILogger<AccountsController> _logger;

        public AccountsController(IConfiguration configuration, 
                                    ApplicationDbContext applicationDbContext,
                                    ILogger<AccountsController> logger)
		{
			_applicationDbContext = applicationDbContext;
            _configuration = configuration;
            _logger = logger;

        }

        public async Task Google()
        {

            await HttpContext.ChallengeAsync(GoogleDefaults.AuthenticationScheme, new AuthenticationProperties()
            {
                RedirectUri = Url.Action("GoogleResponse")
            });

        }

        public IActionResult Login()
		{
            return View();
		}
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(LoginModel loginModel)
        {

            var captchaResponse = await VerifyCaptcha(loginModel.Token);
            if (!captchaResponse)
            {
                ModelState.AddModelError(string.Empty, "CAPTCHA verification failed.");
                return View(loginModel);
            }

            var user = _applicationDbContext.login.FirstOrDefault(u => u.Email == loginModel.Email && u.Password == loginModel.Password);
            if (user == null)
            {
                ViewData["UserNotFoundError"] = "User doesn't exist.";
                return View(loginModel);
            }

            var routeValues = new { name = user.Name, isAdmin = user.isAdmin, nid = user.NID };

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim("UserId", user.Id.ToString()),
                new Claim("UserName", user.Name),
                new Claim("IsAdmin", user.isAdmin.ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
               _configuration["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddDays(30),
                signingCredentials: creds);

            var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

            _logger.LogInformation($"Generated Token: {tokenString}");


            if (user.isAdmin)
            {
                return RedirectToAction("Index", "Home", routeValues);
            }

            return RedirectToAction("UserLogin", "Accounts", routeValues);



        }

        public async Task<bool> VerifyCaptcha(string token)
        {
            try
            {
                var secretKey = "6LciTbQpAAAAAAQZq8mWaytH5-3Y82Hs6_Vll_wi"; 
                var url = $"https://www.google.com/recaptcha/api/siteverify?secret={secretKey}&response={token}";

                using(var client = new HttpClient())
                {
                    var httpResult = await client.GetAsync(url);
                    if(httpResult.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        return false;
                    }
                    var response = await httpResult.Content.ReadAsStringAsync();

                    return true;
                }

            } catch(Exception e)
            {
                return false;
            }

        }

        [HttpGet]
        public async Task GoogleRequest()
        {
             await HttpContext.ChallengeAsync(GoogleDefaults.AuthenticationScheme, new AuthenticationProperties()
            {
                RedirectUri = Url.Action("GoogleResponse")
            });
        }

        public async Task<IActionResult> GoogleResponse()
        {
            var result = await HttpContext.AuthenticateAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        //    var claims = result.Principal.Identities
         //       .FirstOrDefault().Claims.Select(claim => new
        //        {
        //            claim.Issuer,
        //            claim.OriginalIssuer,
         //           claim.Type,
         //           claim.Value
         //       });
            return RedirectToAction("Login");
        }

        public IActionResult UserLogin(string name, bool isAdmin, string nid)
        {
            ViewData["UserName"] = name;
            ViewData["isAdmin"] = isAdmin; //false
            ViewData["NID"] = nid;
            return View();
        }

        public IActionResult Register()
        {
            return View(); 
        }
        [HttpPost]
        public IActionResult Register(LoginModel loginModel)
        {
            if (ModelState.IsValid)
            {
                _applicationDbContext.login.Add(loginModel);
                _applicationDbContext.SaveChanges();
                return RedirectToAction("Login"); 
            }
            return View(loginModel); 
        }
        [HttpGet]
        public IActionResult Invoice([FromQuery] bool isAdmin, [FromQuery] string nid, [FromQuery] string name)
        {

            if (isAdmin)
            {
                IEnumerable<Invoice> invoiceList = _applicationDbContext.invoice;
                return PartialView("Invoice", invoiceList);
            }

            //add route isAdmin
            IEnumerable<Invoice> invoiceListOfUser = _applicationDbContext.invoice.Where(i => i.NID == nid).ToList();
            return PartialView("InvoiceUser", invoiceListOfUser);

        }

        [HttpGet]
        public ActionResult EditInvoice([FromQuery] int id)
        {
            var invoice = _applicationDbContext.invoice.Find(id);

            return View(invoice);
        }


    }
}
