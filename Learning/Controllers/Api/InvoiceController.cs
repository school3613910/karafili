﻿using Learning.Models;
using Learning.NewFolder;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Learning.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public InvoiceController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult CreateInvoice([FromBody] Invoice model) 
        {

            // Add the new invoice to the database
            _applicationDbContext.invoice.Add(model);
            _applicationDbContext.SaveChanges();

            // Redirect the user back to the table of invoices
            return RedirectToAction("Invoice", "Accounts", new { isAdmin = true });

        }
        [HttpDelete]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DeleteInvoice([FromQuery] int id)
        {
            try
            {
                var invoice = await _applicationDbContext.invoice.FindAsync(id);

                if (invoice != null)
                {
                    _applicationDbContext.invoice.Remove(invoice);
                    await _applicationDbContext.SaveChangesAsync();
                    return Ok(); // Assuming successful deletion, returning 200 OK
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPut]
        public IActionResult Edit([FromBody] Invoice invoice, [FromQuery] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                if (invoice != null)
                {
                    Console.WriteLine("invoice  {0}", invoice);

                    Console.WriteLine("invoice id {0}", invoice.Id);

                    var existingInvoice = _applicationDbContext.invoice.Find(id);
                    Console.WriteLine("existingInvoiceee {0}", existingInvoice);

                    if (existingInvoice != null)
                    {
                        // Update the properties of the existing invoice object
                        existingInvoice.CustomerName = invoice.CustomerName;
                        existingInvoice.DateOfCreation = invoice.DateOfCreation;
                        existingInvoice.DueDate = invoice.DueDate;
                        existingInvoice.Amount = invoice.Amount;
                        existingInvoice.Status = invoice.Status;

                        Console.WriteLine("existingInvoice inside  {0}", existingInvoice);


                        _applicationDbContext.Update(existingInvoice);


                        // Save the changes to the database
                        _applicationDbContext.SaveChanges();
                    }
                    return Ok(); // Return a success status code (200 OK)
                }
                else
                {
                    return BadRequest("Invoice object is null");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred: {0}", ex.Message);
                return StatusCode(500, "An error occurred while processing your request.");
            }
        }


        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route("test")]
        public IActionResult Test()
        {
            return Ok();
        }
    }
}
