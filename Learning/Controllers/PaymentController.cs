﻿using Learning.NewFolder;
using Microsoft.AspNetCore.Mvc;
using Stripe;
using Stripe.Checkout;
using System.Security.Cryptography;
using System.Xml.Linq;

namespace Learning.Controllers
{
    public class PaymentController : Controller
    {

        private readonly ApplicationDbContext _applicationDbContext;

        public PaymentController(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public IActionResult CreateCheckoutSession(string invoiceId, decimal amount, string name , string nid )
        {

            //make it to show the amoaun tto be payed 

            var options = new SessionCreateOptions
            {
                PaymentMethodTypes = new List<string>
        {
            "card",
        },
                LineItems = new List<SessionLineItemOptions>
        {
            new SessionLineItemOptions
            {
                PriceData = new SessionLineItemPriceDataOptions
                {
                    UnitAmount = (long)amount * 100, // Example amount in cents
                    Currency = "usd", // Example currency
                    ProductData = new SessionLineItemPriceDataProductDataOptions
                    {
                        Name = "Invoice #" + invoiceId,
                    },
                },
                Quantity = 1,
            },
        },
                Mode = "payment", // Specify the mode as "payment"
                SuccessUrl = $"https://localhost:7152/Payment/UpdatePaymentStatus?invoiceId={invoiceId}&name={name}&nid={nid}",
                CancelUrl = $"https://localhost:7152/Accounts/UserLogin?name={name}&isAdmin=false&nid={nid}",    
            };

            var service = new SessionService();
            var session = service.Create(options);

            return Redirect(session.Url);
        }

        public IActionResult UpdatePaymentStatus( int invoiceId, string name, string nid)
        {

            var invoice = _applicationDbContext.invoice.Find(invoiceId);

            if (invoice != null)
            {
                 invoice.Status = "Paid";
                _applicationDbContext.SaveChanges();
            }
            return Redirect($"https://localhost:7152/Accounts/UserLogin?name={name}&isAdmin=false&nid={nid}");
        }


    }
}
