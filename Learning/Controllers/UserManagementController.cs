﻿using Learning.Models;
using Learning.NewFolder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Learning.Controllers
{
    public class UserManagementController : Controller
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public UserManagementController(UserManager<ApplicationUser> userManager,
                                        RoleManager<IdentityRole> roleManager,
                                        ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [HttpGet]
        public IActionResult UserManagement()
        {
            IEnumerable<LoginModel> usersList = _applicationDbContext.login;
            return PartialView("UserManagement", usersList);
        }

        [HttpPost]
        public async Task<IActionResult> AddUser(string username, string role, string email, string password, string address, string phone)
        {
            var user = new ApplicationUser
            {
                UserName = username,
                Email = email
            };

            var result = await _userManager.CreateAsync(user, password);
            if (result.Succeeded)
            {
                // Add user to the specified role
                if (!await _roleManager.RoleExistsAsync(role))
                {
                    await _roleManager.CreateAsync(new IdentityRole(role));
                }

                await _userManager.AddToRoleAsync(user, role);

                // Optionally, save additional user details such as address and phone to your custom user model

                return Ok();
            }
            else
            {
                return BadRequest(result.Errors);
            }
        }
    }
}
