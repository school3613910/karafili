﻿using Learning.Models;
using Learning.NewFolder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

public class DashboardController : Controller
{
    private readonly ApplicationDbContext _context;

    public DashboardController(ApplicationDbContext context)
    {
        _context = context;
    }

    public IActionResult GetInvoiceData()
    {

        var currentTime = DateTime.UtcNow;
        var cookieName = "dashboardStart";

        string cookieValue = Request.Cookies[cookieName];


        if (cookieValue == null)
        {
     
                // Cookie has expired, clear it and end the session
                Response.Cookies.Delete(cookieName);
                // Handle session ending logic here, like redirecting to login or showing a session expired message
                return RedirectToAction("Login", "Accounts");
           
        }


        // Retrieve actual data from the database
        var invoices = _context.invoice.ToList();

        var paid = invoices.Where(i => i.Status == "Paid").Sum(i => i.Amount);
        var unpaid = invoices.Where(i => i.Status == "Unpaid").Sum(i => i.Amount);

        return Json(new { paid, unpaid });
    }

    public IActionResult Dashboard()
    {
        return PartialView("Dashboard");
    }
}
