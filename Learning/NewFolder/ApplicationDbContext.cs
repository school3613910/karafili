﻿using Learning.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Learning.NewFolder
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>

    {
        public ApplicationDbContext(DbContextOptions options) :base(options)
        {

        }

        public DbSet<Category> category { get; set; }
        public DbSet<LoginModel> login { get; set; }
        public DbSet<Invoice> invoice { get; set; }
    }
}
