﻿using System.ComponentModel.DataAnnotations;

namespace Learning.Models
{
    public class Invoice
    {
        [Key]
        public int Id { get; set; }
        public string? CustomerName { get; set; }
        public DateTime? DateOfCreation { get; set; }
        public DateTime? DueDate { get; set; }
        public decimal? Amount { get; set; }
        public string? Status { get; set; }
        public string? NID { get; set; }


    }
}
