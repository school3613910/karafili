﻿using System.ComponentModel.DataAnnotations;

namespace Learning.Models
{
	public class LoginModel
	{
        [Key]
        public int Id { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [StringLength(35, MinimumLength = 5, ErrorMessage = "Email must be between 5 and 35 characters")]
        public string? Email { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [RegularExpression(@"^(?!.*[`])(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$",
            ErrorMessage = "Password must be at least 8 characters long, contain at least one uppercase letter, one lowercase letter, one digit, and one special character.")]
        public string? Password { get; set; }
        public string? NID { get; set; }
        public string? Name { get; set; }
        public bool isAdmin {  get; set; }
        [Required]
        public string? Token { get; set; }
        public int? Amount { get; set; }
        public int? Age { get; set; }

        public string? Gender { get; set; }

        [Phone]
        public string? CellPhone { get; set; }

        public string? City { get; set; }

        public string? State { get; set; }

    }
}
