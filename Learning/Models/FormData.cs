﻿namespace Learning.Models
{
    public class FormData
    {
        public bool IsAdmin { get; set; }
        public string Nid { get; set; }
        public string Name { get; set; }
        // Add other properties here if needed
    }
}
